# api-cadastro-cliente

API para cadastro de cliente construída em Java.

Para executar a aplicação o arquivo application.properties deve ser editado, as propriedades spring.datasource.username, spring.datasource.password e spring.datasource.url devem ser
ajustadas para o banco de dados da maquina onde será executada. Caso o banco não seja Postgres a propriedade spring.datasource.driver-class-name deve ser ajustada com o driver do 
banco requerido. Após isto basta executar a aplicação que esta por default na porta 8080.