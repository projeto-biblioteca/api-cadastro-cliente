package br.com.buges.projetobiblioteca.apicadastroclientes.core.usecase.dto;

public enum ListaErroEnum {
	CAMPOS_OBRIGATORIOS, DUPLICIDADE, OUTROS, ENTIDADE_NAO_ENCONTRADA;
}
