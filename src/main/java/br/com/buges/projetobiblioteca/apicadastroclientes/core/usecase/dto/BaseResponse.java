package br.com.buges.projetobiblioteca.apicadastroclientes.core.usecase.dto;

public class BaseResponse {

	private ResponseData response = new ResponseData();

	public ResponseData getResponse() {
		return response;
	}

	public void setResponse(ResponseData response) {
		this.response = response;
	}
}
