package br.com.buges.projetobiblioteca.apicadastroclientes.core.usecase;

import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Service;

import br.com.buges.projetobiblioteca.apicadastroclientes.core.entity.Cliente;
import br.com.buges.projetobiblioteca.apicadastroclientes.core.usecase.dto.ClienteResponse;
import br.com.buges.projetobiblioteca.apicadastroclientes.core.usecase.dto.ListaErroEnum;
import br.com.buges.projetobiblioteca.apicadastroclientes.core.usecase.dto.ResponseDataErro;
import br.com.buges.projetobiblioteca.apicadastroclientes.core.usecase.gateway.BuscarTodosClienteGateway;

@Service
public class BuscarTodosUseCaseImpl implements BuscarTodosUseCase {

	private final BuscarTodosClienteGateway buscarTodosClienteGateway;

	public BuscarTodosUseCaseImpl(BuscarTodosClienteGateway buscarTodosClienteGateway) {
		this.buscarTodosClienteGateway = buscarTodosClienteGateway;
	}

	@Override
	public List<ClienteResponse> executar(Object input) {

		List<ClienteResponse> listResponse = new ArrayList<ClienteResponse>();
		List<Cliente> clientes = buscarTodosClienteGateway.BuscarTodosCliente();

		if (!clientes.isEmpty()) {
			for (Cliente cliente : clientes) {
				ClienteResponse responseCliente = new ClienteResponse();

				responseCliente.setNome(cliente.getNome());
				responseCliente.setId(cliente.getId());
				responseCliente.setTelefone(cliente.getTelefone());
				responseCliente.setDataNascimento(cliente.getDataNascimento());

				listResponse.add(responseCliente);
			}
		} else {
			ClienteResponse responseCliente = new ClienteResponse();
			responseCliente.getResponse().adicionarErro(
					new ResponseDataErro("Nenhum cliente cadastrado", ListaErroEnum.ENTIDADE_NAO_ENCONTRADA));

			listResponse.add(responseCliente);
		}

		return listResponse;
	}

}
