package br.com.buges.projetobiblioteca.apicadastroclientes.core.usecase;

public interface BaseUseCase<I, O> {

	O executar (I input);
}
