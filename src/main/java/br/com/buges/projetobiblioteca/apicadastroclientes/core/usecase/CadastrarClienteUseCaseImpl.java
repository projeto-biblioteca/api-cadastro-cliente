package br.com.buges.projetobiblioteca.apicadastroclientes.core.usecase;

import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Service;

import br.com.buges.projetobiblioteca.apicadastroclientes.core.entity.Cliente;
import br.com.buges.projetobiblioteca.apicadastroclientes.core.usecase.dto.ClienteRequest;
import br.com.buges.projetobiblioteca.apicadastroclientes.core.usecase.dto.ClienteResponse;
import br.com.buges.projetobiblioteca.apicadastroclientes.core.usecase.dto.ListaErroEnum;
import br.com.buges.projetobiblioteca.apicadastroclientes.core.usecase.dto.ResponseDataErro;
import br.com.buges.projetobiblioteca.apicadastroclientes.core.usecase.gateway.BuscarPorCpfClienteGateway;
import br.com.buges.projetobiblioteca.apicadastroclientes.core.usecase.gateway.SalvarGateway;

@Service
public class CadastrarClienteUseCaseImpl implements CadastrarClienteUseCase {

	private final SalvarGateway<Cliente> salvarGateway;
	private final BuscarPorCpfClienteGateway buscarPorCpfClienteGateway;

	public CadastrarClienteUseCaseImpl(SalvarGateway<Cliente> salvarGateway,
			BuscarPorCpfClienteGateway buscarPorCpfClienteGateway) {
		this.salvarGateway = salvarGateway;
		this.buscarPorCpfClienteGateway = buscarPorCpfClienteGateway;
	}

	@Override
	public ClienteResponse executar(ClienteRequest input) {
		ClienteResponse response = new ClienteResponse();
		response.setNome(input.getNome());
		response.setCpf(input.getCpf());
		response.setDataNascimento(input.getDataNascimento());
		response.setTelefone(input.getTelefone());
		
		ValidarCamposObrigatorios(input, response);
		
		if (buscarPorCpfClienteGateway.buscarPorCpfCliente(input.getCpf()).isPresent()) {
			response.getResponse().adicionarErro(new ResponseDataErro("Cliente ja cadastrado com CPF: " + input.getCpf(), ListaErroEnum.DUPLICIDADE));
		} else {
			Cliente cliente = new Cliente();
			
			cliente.setNome(input.getNome());
			cliente.setCpf(input.getCpf());
			cliente.setDataNascimento(input.getDataNascimento());
			cliente.setTelefone(input.getTelefone());
			salvarGateway.Salvar(cliente);
			response.setId(cliente.getId());
		}
		
		return response;
	}
	
	private void ValidarCamposObrigatorios(ClienteRequest input, ClienteResponse response) {
		
		if (input.getCpf() <= 0) {
			response.getResponse().adicionarErro(new ResponseDataErro("O campo cpf é obrigatorio", ListaErroEnum.CAMPOS_OBRIGATORIOS));
		}
		
		if (StringUtils.isBlank(input.getNome())) {
			response.getResponse().adicionarErro(new ResponseDataErro("O campo nome é obrigatorio", ListaErroEnum.CAMPOS_OBRIGATORIOS));
		}
	}
}
