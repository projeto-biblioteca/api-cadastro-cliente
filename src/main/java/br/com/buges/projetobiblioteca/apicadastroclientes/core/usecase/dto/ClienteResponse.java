package br.com.buges.projetobiblioteca.apicadastroclientes.core.usecase.dto;

import java.time.LocalDate;

public class ClienteResponse extends BaseResponse {

	public ClienteResponse() {
		super();
	}
	
	public ClienteResponse(String id, String nome, Long cpf, String telefone,
			LocalDate dataNascimento) {
		super();
		this.id = id;
		this.nome = nome;
		this.cpf = cpf;
		this.telefone = telefone;
		this.dataNascimento = dataNascimento;
	}

	private String id;
	private String nome;
	private Long cpf;
	private String telefone;
	private LocalDate dataNascimento;
	
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getNome() {
		return nome;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}
	public Long getCpf() {
		return cpf;
	}
	public void setCpf(Long cpf) {
		this.cpf = cpf;
	}
	public String getTelefone() {
		return telefone;
	}
	public void setTelefone(String telefone) {
		this.telefone = telefone;
	}
	public LocalDate getDataNascimento() {
		return dataNascimento;
	}
	public void setDataNascimento(LocalDate dataNascimento) {
		this.dataNascimento = dataNascimento;
	}
}
