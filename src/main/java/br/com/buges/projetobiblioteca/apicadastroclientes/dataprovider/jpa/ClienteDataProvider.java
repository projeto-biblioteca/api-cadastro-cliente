package br.com.buges.projetobiblioteca.apicadastroclientes.dataprovider.jpa;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.Optional;

import org.springframework.stereotype.Repository;

import br.com.buges.projetobiblioteca.apicadastroclientes.configuration.mapper.ObjectMapperUtil;
import br.com.buges.projetobiblioteca.apicadastroclientes.core.entity.Cliente;
import br.com.buges.projetobiblioteca.apicadastroclientes.core.usecase.gateway.BuscarPorCpfClienteGateway;
import br.com.buges.projetobiblioteca.apicadastroclientes.core.usecase.gateway.BuscarPorIdClienteGateway;
import br.com.buges.projetobiblioteca.apicadastroclientes.core.usecase.gateway.BuscarTodosClienteGateway;
import br.com.buges.projetobiblioteca.apicadastroclientes.core.usecase.gateway.SalvarGateway;
import br.com.buges.projetobiblioteca.apicadastroclientes.dataprovider.jpa.entity.JpaClienteEntity;
import br.com.buges.projetobiblioteca.apicadastroclientes.dataprovider.jpa.repository.JpaClienteRepository;

@Repository
public class ClienteDataProvider implements BuscarTodosClienteGateway, BuscarPorCpfClienteGateway, SalvarGateway<Cliente>, BuscarPorIdClienteGateway<Cliente, String>{

	private final JpaClienteRepository repository;
	
	public ClienteDataProvider(JpaClienteRepository repository) {
		this.repository = repository;
	}
	
	@Override
	public Optional<Cliente> buscarPorIdCliente(String id) {

		JpaClienteEntity jpaClienteEntity = repository.findById(id).orElse(null);
		
		if(Objects.nonNull(jpaClienteEntity)) {
			return Optional.of(ObjectMapperUtil.convertTo(jpaClienteEntity, Cliente.class));
		} else {
			return Optional.empty();
		}
	}

	@Override
	public Cliente Salvar(Cliente t) {
		
		JpaClienteEntity jpaClienteEntity = repository.save(ObjectMapperUtil.convertTo(t, JpaClienteEntity.class));
		
		return ObjectMapperUtil.convertTo(jpaClienteEntity, Cliente.class);
	}

	@Override
	public Optional<Cliente> buscarPorCpfCliente(Long cpf) {
		
		JpaClienteEntity jpaClienteEntity = repository.findByCpf(cpf).orElse(null);
		
		if(Objects.nonNull(jpaClienteEntity)) {
			return Optional.of(ObjectMapperUtil.convertTo(jpaClienteEntity, Cliente.class));
		} else {
			return Optional.empty();
		}	
	}

	@Override
	public List<Cliente> BuscarTodosCliente() {

		List<JpaClienteEntity> jpaClienteEntity = repository.findAll();
		List<Cliente> clientes = new ArrayList<Cliente>();
		
		if(!jpaClienteEntity.isEmpty()) {
			jpaClienteEntity.forEach(cliente->clientes.add(ObjectMapperUtil.convertTo(jpaClienteEntity, Cliente.class))); 
			return clientes;
		}else {
			return clientes;
		}
		
	}
	
}
