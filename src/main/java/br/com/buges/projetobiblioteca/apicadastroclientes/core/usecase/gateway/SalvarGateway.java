package br.com.buges.projetobiblioteca.apicadastroclientes.core.usecase.gateway;

public interface SalvarGateway<T> {
	
	T Salvar (T t);

}
