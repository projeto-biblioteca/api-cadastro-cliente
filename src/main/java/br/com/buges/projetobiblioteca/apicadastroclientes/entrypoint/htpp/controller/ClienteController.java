package br.com.buges.projetobiblioteca.apicadastroclientes.entrypoint.htpp.controller;

import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import br.com.buges.projetobiblioteca.apicadastroclientes.core.usecase.BaseUseCase;
import br.com.buges.projetobiblioteca.apicadastroclientes.core.usecase.BuscarPorCpfUseCase;
import br.com.buges.projetobiblioteca.apicadastroclientes.core.usecase.BuscarTodosUseCase;
import br.com.buges.projetobiblioteca.apicadastroclientes.core.usecase.dto.ClienteRequest;
import br.com.buges.projetobiblioteca.apicadastroclientes.core.usecase.dto.ClienteResponse;

@RestController
@RequestMapping("/api/v1/clientes")
public class ClienteController {
	
	private final BaseUseCase<ClienteRequest, ClienteResponse> useCase;
	private final BuscarPorCpfUseCase buscaPorCpfUseCase;
	private final BuscarTodosUseCase<String> buscarTodosUseCase;

	public ClienteController(BaseUseCase<ClienteRequest, ClienteResponse> uc, 
			BuscarPorCpfUseCase buscaPorCpfUseCase,
			BuscarTodosUseCase<String> buscarTodosUseCase) {
		this.useCase = uc;
		this.buscaPorCpfUseCase = buscaPorCpfUseCase;
		this.buscarTodosUseCase = buscarTodosUseCase;
	}

	@CrossOrigin(origins = "http://localhost:4200")
	@PostMapping(consumes = MediaType.APPLICATION_JSON_UTF8_VALUE, produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
	public ResponseEntity<ClienteResponse> cadastrarCliente(@RequestBody ClienteRequest request) {
		
		ClienteResponse response = useCase.executar(request);
		return ResponseEntity.ok(response);
	}
	
	@CrossOrigin(origins = "http://localhost:4200")
	@GetMapping("/{cpf}")
	public ResponseEntity<ClienteResponse> buscarPorCpf(@PathVariable("cpf") Long cpf) {
		
		ClienteResponse response = buscaPorCpfUseCase.executar(cpf);
		return ResponseEntity.ok(response);
	}

}
