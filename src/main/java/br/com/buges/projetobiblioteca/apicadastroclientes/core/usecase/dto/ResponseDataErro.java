package br.com.buges.projetobiblioteca.apicadastroclientes.core.usecase.dto;

public class ResponseDataErro {
	private String mensage;
	private ListaErroEnum tipo;
	
	public String getMensage() {
		return mensage;
	}
	public void setMensage(String mensage) {
		this.mensage = mensage;
	}
	public ListaErroEnum getTipo() {
		return tipo;
	}
	public void setTipo(ListaErroEnum tipo) {
		this.tipo = tipo;
	}
	
	public ResponseDataErro(String mensage, ListaErroEnum tipo) {
		this.mensage = mensage;
		this.tipo = tipo;
	}

}
