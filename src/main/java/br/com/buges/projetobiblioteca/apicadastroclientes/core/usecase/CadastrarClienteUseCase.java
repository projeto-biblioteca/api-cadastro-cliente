package br.com.buges.projetobiblioteca.apicadastroclientes.core.usecase;

import br.com.buges.projetobiblioteca.apicadastroclientes.core.usecase.dto.ClienteRequest;
import br.com.buges.projetobiblioteca.apicadastroclientes.core.usecase.dto.ClienteResponse;


public interface CadastrarClienteUseCase extends BaseUseCase<ClienteRequest, ClienteResponse> {
}
