package br.com.buges.projetobiblioteca.apicadastroclientes.core.usecase.gateway;

import java.util.List;

import br.com.buges.projetobiblioteca.apicadastroclientes.core.entity.Cliente;

public interface BuscarTodosClienteGateway {

	List<Cliente> BuscarTodosCliente();
}
 