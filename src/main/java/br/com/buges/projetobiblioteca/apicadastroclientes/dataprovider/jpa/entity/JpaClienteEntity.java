package br.com.buges.projetobiblioteca.apicadastroclientes.dataprovider.jpa.entity;

import java.time.LocalDate;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;


@Entity
@Table(name = "cliente")
public class JpaClienteEntity extends JpaBaseEntity {
	
	@Column(name = "nome", length = 40, nullable = false)
	private String nome;
	@Column(name = "cpf", length = 20, nullable = false)
	private Long cpf;
	@Column(name = "telefone", length = 13, nullable = false)
	private String telefone;
	@Column(name = "dataNascimento", nullable = false)
	private LocalDate dataNascimento;
	
	public String getNome() {
		return nome;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}
	public Long getCpf() {
		return cpf;
	}
	public void setCpf(Long cpf) {
		this.cpf = cpf;
	}
	public String getTelefone() {
		return telefone;
	}
	public void setTelefone(String telefone) {
		this.telefone = telefone;
	}
	public LocalDate getDataNascimento() {
		return dataNascimento;
	}
	public void setDataNascimento(LocalDate dataNascimento) {
		this.dataNascimento = dataNascimento;
	}

}
