package br.com.buges.projetobiblioteca.apicadastroclientes.core.usecase;

import java.util.Optional;

import org.springframework.stereotype.Service;

import br.com.buges.projetobiblioteca.apicadastroclientes.core.entity.Cliente;
import br.com.buges.projetobiblioteca.apicadastroclientes.core.usecase.dto.ClienteResponse;
import br.com.buges.projetobiblioteca.apicadastroclientes.core.usecase.dto.ListaErroEnum;
import br.com.buges.projetobiblioteca.apicadastroclientes.core.usecase.dto.ResponseDataErro;
import br.com.buges.projetobiblioteca.apicadastroclientes.core.usecase.gateway.BuscarPorCpfClienteGateway;

@Service
public class BuscarPorCpfUseCaseImpl implements BuscarPorCpfUseCase {

	private final BuscarPorCpfClienteGateway buscarPorCpfClienteGateway;
	
	public BuscarPorCpfUseCaseImpl(BuscarPorCpfClienteGateway buscarPorCpfClienteGateway) {
		this.buscarPorCpfClienteGateway = buscarPorCpfClienteGateway;
	}

	@Override
	public ClienteResponse executar(Long cpf) {

		ClienteResponse response = new ClienteResponse();
		response.setCpf(cpf);

		Optional<Cliente> opCliente = buscarPorCpfClienteGateway.buscarPorCpfCliente(cpf);

		if (opCliente.isPresent()) {
			response.setNome(opCliente.get().getNome());
			response.setId(opCliente.get().getId());
			response.setTelefone(opCliente.get().getTelefone());
			response.setDataNascimento(opCliente.get().getDataNascimento());
		} else {
			response.getResponse().adicionarErro(new ResponseDataErro("Cliente não encontrado com CPF " + cpf,
					ListaErroEnum.ENTIDADE_NAO_ENCONTRADA));
		}

		return response;
	}
	
}
