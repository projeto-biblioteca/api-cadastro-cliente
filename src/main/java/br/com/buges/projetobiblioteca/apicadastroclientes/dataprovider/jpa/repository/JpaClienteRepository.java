package br.com.buges.projetobiblioteca.apicadastroclientes.dataprovider.jpa.repository;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import br.com.buges.projetobiblioteca.apicadastroclientes.dataprovider.jpa.entity.JpaClienteEntity;

public interface JpaClienteRepository extends JpaRepository<JpaClienteEntity, String>{

	Optional<JpaClienteEntity> findByCpf(Long cpf);
}
