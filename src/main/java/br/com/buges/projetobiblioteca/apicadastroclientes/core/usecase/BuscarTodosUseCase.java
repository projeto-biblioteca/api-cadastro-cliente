package br.com.buges.projetobiblioteca.apicadastroclientes.core.usecase;

import br.com.buges.projetobiblioteca.apicadastroclientes.core.usecase.dto.ClienteResponse;

public interface BuscarTodosUseCase<T> extends BaseUseCase<T, ClienteResponse> {

}
