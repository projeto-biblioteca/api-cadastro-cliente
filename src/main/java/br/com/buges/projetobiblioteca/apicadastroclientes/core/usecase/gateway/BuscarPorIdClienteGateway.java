package br.com.buges.projetobiblioteca.apicadastroclientes.core.usecase.gateway;

import java.util.Optional;

public interface BuscarPorIdClienteGateway<T, ID> {

	Optional<T> buscarPorIdCliente(ID id);
	
}
