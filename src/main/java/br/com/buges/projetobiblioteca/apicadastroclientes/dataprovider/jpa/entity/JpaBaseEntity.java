package br.com.buges.projetobiblioteca.apicadastroclientes.dataprovider.jpa.entity;

import java.util.Objects;

import javax.persistence.Column;
import javax.persistence.Id;
import javax.persistence.MappedSuperclass;

@MappedSuperclass
public abstract class JpaBaseEntity {

	@Id
	@Column(name = "id", length = 40, nullable = false)
	private String id;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	@Override
	public int hashCode() {
		return Objects.hash(id);
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		JpaBaseEntity other = (JpaBaseEntity) obj;
		return Objects.equals(id, other.id);
	}
}
