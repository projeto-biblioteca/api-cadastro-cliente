package br.com.buges.projetobiblioteca.apicadastroclientes.core.usecase;

import br.com.buges.projetobiblioteca.apicadastroclientes.core.usecase.dto.ClienteResponse;

public interface BuscarPorCpfUseCase extends BaseUseCase<Long, ClienteResponse> {

}
