package br.com.buges.projetobiblioteca.apicadastroclientes.core.usecase.gateway;

import java.util.Optional;

import br.com.buges.projetobiblioteca.apicadastroclientes.core.entity.Cliente;

public interface BuscarPorNomeClienteGateway {

	Optional<Cliente> buscarPorNomeCliente(String nome);
}
